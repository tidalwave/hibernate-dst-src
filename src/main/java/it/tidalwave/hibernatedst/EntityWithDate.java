/*
 * #%L
 * *********************************************************************************************************************
 *
 * git clone https://tidalwave@bitbucket.org/tidalwave/hibernate-dst-src.git
 * %%
 * Copyright (C) 2015 - 2015 Tidalwave s.a.s. (http://tidalwave.it)
 * %%
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * $Id$
 *
 * *********************************************************************************************************************
 * #L%
 */
package it.tidalwave.hibernatedst;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;
import javax.persistence.Access;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;
import static javax.persistence.AccessType.FIELD;

/***********************************************************************************************************************
 *
 * @author  Fabrizio Giudici Fabrizio.Giudici@tidalwave.it
 * @version $Id$
 *
 **********************************************************************************************************************/
@Entity
@Access(FIELD)
@Table(name = "EWD")
public class EntityWithDate implements Serializable
  {
    private static final long serialVersionUID = 3377846109713145331L;

    @Id
    @Column(name = "ID", length = 36)
    private String id = UUID.randomUUID().toString();

    @Column(name = "DT")
    @Type(type = "it.tidalwave.hibernatedst.LocalDateTimeUserType6")
    private LocalDateTime dateTime;

    public String getId()
      {
        return id;
      }

    public LocalDateTime getDateTime()
      {
        return dateTime;
      }

    public void setDateTime (final LocalDateTime dateTime)
      {
        this.dateTime = dateTime;
      }

    @Override
    public int hashCode()
      {
        return Objects.hashCode(id);
      }

    @Override
    public boolean equals (final Object object)
      {
        if (this == object)
          {
            return true;
          }

        if ((object == null) || !(object instanceof EntityWithDate))
          {
            return false;
          }

        final EntityWithDate other = (EntityWithDate)object;
        return Objects.equals(this.id, other.id);
      }
  }
