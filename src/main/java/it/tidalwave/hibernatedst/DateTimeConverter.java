/*
 * #%L
 * *********************************************************************************************************************
 *
 * git clone https://tidalwave@bitbucket.org/tidalwave/hibernate-dst-src.git
 * %%
 * Copyright (C) 2015 - 2015 Tidalwave s.a.s. (http://tidalwave.it)
 * %%
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * $Id$
 *
 * *********************************************************************************************************************
 * #L%
 */
package it.tidalwave.hibernatedst;

import org.joda.time.LocalDateTime;

/***********************************************************************************************************************
 *
 * This interface is only required for tests in order to have some common ground for the alternate implementations.
 *
 * @author  Fabrizio Giudici (Fabrizio.Giudici@tidalwave.it)
 * @version $Id$
 *
 **********************************************************************************************************************/
public interface DateTimeConverter<T>
  {
    public LocalDateTime toLocalDateTime (final T object);

    public T fromLocalDateTime (final LocalDateTime localDateTime);
  }
