/*
 * #%L
 * *********************************************************************************************************************
 *
 * git clone https://tidalwave@bitbucket.org/tidalwave/hibernate-dst-src.git
 * %%
 * Copyright (C) 2015 - 2015 Tidalwave s.a.s. (http://tidalwave.it)
 * %%
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * $Id$
 *
 * *********************************************************************************************************************
 * #L%
 */
package it.tidalwave.hibernatedst;

import java.sql.Timestamp;
import java.sql.Types;
import org.hibernate.type.StandardBasicTypes;
import org.joda.time.LocalDateTime;

/***********************************************************************************************************************
 *
 * Without the default conversion LocalDateTime <-> Timestamp the test fails in transition hours.
 *
 * @author  Fabrizio Giudici Fabrizio.Giudici@tidalwave.it
 * @version $Id$
 *
 **********************************************************************************************************************/
public class LocalDateTimeUserType1 extends LocalDateTimeUserTypeSupport<Timestamp>
  {
    private static final long serialVersionUID = -910771091722534570L;

    public LocalDateTimeUserType1()
      {
        super(LocalDateTime.class, Types.TIMESTAMP, StandardBasicTypes.TIMESTAMP);
      }

    @Override
    public LocalDateTime toLocalDateTime (final Timestamp timestamp)
      {
        return (timestamp == null) ? null : new LocalDateTime(timestamp.getTime());
      }

    @Override
    public Timestamp fromLocalDateTime (final LocalDateTime localDateTime)
      {
        return (localDateTime == null) ? null : new Timestamp(localDateTime.toDate().getTime());
      }
  }
