/*
 * #%L
 * *********************************************************************************************************************
 *
 * git clone https://tidalwave@bitbucket.org/tidalwave/hibernate-dst-src.git
 * %%
 * Copyright (C) 2015 - 2015 Tidalwave s.a.s. (http://tidalwave.it)
 * %%
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * $Id$
 *
 * *********************************************************************************************************************
 * #L%
 */
package it.tidalwave.hibernatedst;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.type.AbstractStandardBasicType;
import org.joda.time.LocalDateTime;

/***********************************************************************************************************************
 *
 * @author  Fabrizio Giudici Fabrizio.Giudici@tidalwave.it
 * @version $Id$
 *
 **********************************************************************************************************************/
public abstract class LocalDateTimeUserTypeSupport<T> extends ImmutableUserTypeSupport implements DateTimeConverter<T>
  {
    private static final long serialVersionUID = -910771091722534570L;

    private final AbstractStandardBasicType basicType;

    public LocalDateTimeUserTypeSupport (Class<?> typeClass, int type, AbstractStandardBasicType basicType)
      {
        super(typeClass, type);
        this.basicType = basicType;
      }

    @Override
    public Object nullSafeGet (ResultSet resultSet, String[] names, SessionImplementor session, Object owner)
      throws HibernateException, SQLException
      {
        final T object = (T)basicType.nullSafeGet(resultSet, names, session, owner);
        return toLocalDateTime(object);
      }

    @Override
    public void nullSafeSet (PreparedStatement preparedStatement, Object value, int index, SessionImplementor session)
      throws HibernateException, SQLException
      {
        final T object = fromLocalDateTime((LocalDateTime)value);
        basicType.nullSafeSet(preparedStatement, object, index, session);
      }
  }
