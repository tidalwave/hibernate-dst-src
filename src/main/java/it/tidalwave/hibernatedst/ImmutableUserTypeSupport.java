/*
 * #%L
 * *********************************************************************************************************************
 *
 * git clone https://tidalwave@bitbucket.org/tidalwave/hibernate-dst-src.git
 * %%
 * Copyright (C) 2015 - 2015 Tidalwave s.a.s. (http://tidalwave.it)
 * %%
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * $Id$
 *
 * *********************************************************************************************************************
 * #L%
 */
package it.tidalwave.hibernatedst;

import java.io.Serializable;
import java.util.Objects;
import org.hibernate.HibernateException;
import org.hibernate.usertype.UserType;

/***********************************************************************************************************************
 *
 * @author  Fabrizio Giudici Fabrizio.Giudici@tidalwave.it
 * @version $Id$
 *
 **********************************************************************************************************************/
public abstract class ImmutableUserTypeSupport implements UserType, Serializable
  {
    private static final long serialVersionUID = -4692619064637793846L;

    private final Class<?> typeClass;

    private final int[] sqlTypes;

    public ImmutableUserTypeSupport (final Class<?> typeClass, final int type)
      {
        this.typeClass = typeClass;
        this.sqlTypes = new int[]{ type };
      }

    @Override
    public int[] sqlTypes()
      {
        return sqlTypes;
      }

    @Override
    public Class<?> returnedClass()
      {
        return typeClass;
      }

    @Override
    public boolean equals (final Object o1, final Object o2)
      throws HibernateException
      {
        return Objects.equals(o1, o2);
      }

    @Override
    public int hashCode (final Object object)
      throws HibernateException
      {
        return object.hashCode();
      }

    @Override
    public Object deepCopy (Object value)
      {
        return value;
      }

    @Override
    public boolean isMutable()
      {
        return false;
      }

    @Override
    public Serializable disassemble (Object value)
      {
        return (Serializable)value;
      }

    @Override
    public Object assemble (Serializable cached, Object value)
      throws HibernateException
      {
        return cached;
      }

    @Override
    public Object replace (Object original, Object target, Object owner)
      throws HibernateException
      {
        return original;
      }
  }
