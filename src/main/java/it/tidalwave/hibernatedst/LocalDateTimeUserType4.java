/*
 * #%L
 * *********************************************************************************************************************
 *
 * git clone https://tidalwave@bitbucket.org/tidalwave/hibernate-dst-src.git
 * %%
 * Copyright (C) 2015 - 2015 Tidalwave s.a.s. (http://tidalwave.it)
 * %%
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * $Id$
 *
 * *********************************************************************************************************************
 * #L%
 */
package it.tidalwave.hibernatedst;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.Calendar;
import java.util.TimeZone;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.type.StandardBasicTypes;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDateTime;

/***********************************************************************************************************************
 *
 * This passes the tests, but the time stored into the database is in UTC.
 *
 * @author  Fabrizio Giudici Fabrizio.Giudici@tidalwave.it
 * @version $Id$
 *
 **********************************************************************************************************************/
public class LocalDateTimeUserType4 extends LocalDateTimeUserTypeSupport<Timestamp>
  {
    private static final DateTimeZone DTZ = DateTimeZone.forOffsetHours(0);

    private static final Calendar CALENDAR = Calendar.getInstance(TimeZone.getTimeZone("00:00"));

    private static final long serialVersionUID = -910771091722534570L;

    public LocalDateTimeUserType4()
      {
        super(LocalDateTime.class, Types.TIMESTAMP, StandardBasicTypes.TIMESTAMP);
      }

    @Override
    public Object nullSafeGet (ResultSet rs, String[] names, SessionImplementor session, Object owner)
      throws HibernateException, SQLException
      {
        return toLocalDateTime(rs.getTimestamp(names[0], CALENDAR));
      }

    @Override
    public void nullSafeSet (PreparedStatement st, Object value, int index, SessionImplementor session)
      throws HibernateException, SQLException
      {
        st.setTimestamp(index, fromLocalDateTime((LocalDateTime)value), CALENDAR);
      }

    @Override
    public LocalDateTime toLocalDateTime (final Timestamp timestamp)
      {
        return (timestamp == null) ? null : new LocalDateTime(timestamp.getTime(), DTZ);
      }

    @Override
    public Timestamp fromLocalDateTime (final LocalDateTime localDateTime)
      {
        return (localDateTime == null) ? null : new Timestamp(localDateTime.toDate(DTZ.toTimeZone()).getTime());
      }
  }
