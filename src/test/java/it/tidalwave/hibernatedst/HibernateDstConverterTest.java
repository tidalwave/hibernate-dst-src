/*
 * #%L
 * *********************************************************************************************************************
 *
 * git clone https://tidalwave@bitbucket.org/tidalwave/hibernate-dst-src.git
 * %%
 * Copyright (C) 2015 - 2015 Tidalwave s.a.s. (http://tidalwave.it)
 * %%
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * $Id$
 *
 * *********************************************************************************************************************
 * #L%
 */
package it.tidalwave.hibernatedst;

import org.joda.time.LocalDateTime;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import static org.junit.Assert.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/***********************************************************************************************************************
 *
 * @author  Fabrizio Giudici Fabrizio.Giudici@tidalwave.it
 * @version $Id$
 *
 **********************************************************************************************************************/
public class HibernateDstConverterTest extends DateTimes
  {
    private final static Logger log = LoggerFactory.getLogger(HibernateDstConverterTest.class);

    @DataProvider
    private static Object[][] converterClass()
      {
        return new Object[][]
          {
            { new LocalDateTimeUserType1() },
            { new LocalDateTimeUserType2() },
            { new LocalDateTimeUserType3() },
            { new LocalDateTimeUserType4() },
            { new LocalDateTimeUserType5() },
            { new LocalDateTimeUserType6() },
          };
      }

    @DataProvider
    private static Object[][] dateTimeAndConverterClass()
      {
        final Object[][] o1 = dateTimeProvider();
        final Object[][] o2 = converterClass();

        final Object[][] r = new Object[o1.length * o2.length][2];

        for (int i = 0; i < o1.length; i++)
          {
            for (int j = 0; j < o2.length; j++)
              {
                final int k = i* o2.length + j;
                r[k][0] = o1[i][0];
                r[k][1] = o2[j][0];
              }
          }

        return r;
      };

    @Test(dataProvider = "dateTimeAndConverterClass")
    public <T> void roundtrip_conversion_test (final LocalDateTime expectedLdt, final DateTimeConverter<T> conv)
      {
        final T converted = conv.fromLocalDateTime(expectedLdt);
        final LocalDateTime actualLdt = conv.toLocalDateTime(converted);

        assertEquals(conv.getClass().getSimpleName(), expectedLdt, actualLdt);

//        if (converted instanceof Timestamp)
//          {
//            assertEquals("storage for " + conv.getClass().getSimpleName(), actualLdt.toString("yyyy-MM-dd HH:mm:ss.0"), converted.toString());
//          }
      }
  }
