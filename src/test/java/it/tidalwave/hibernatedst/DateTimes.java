/*
 * #%L
 * *********************************************************************************************************************
 *
 * git clone https://tidalwave@bitbucket.org/tidalwave/hibernate-dst-src.git
 * %%
 * Copyright (C) 2015 - 2015 Tidalwave s.a.s. (http://tidalwave.it)
 * %%
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * $Id$
 *
 * *********************************************************************************************************************
 * #L%
 */
package it.tidalwave.hibernatedst;

import org.joda.time.LocalDateTime;
import org.testng.annotations.DataProvider;

/***********************************************************************************************************************
 *
 * @author  Fabrizio Giudici (Fabrizio.Giudici@tidalwave.it)
 * @version $Id$
 *
 **********************************************************************************************************************/
public class DateTimes
  {
    @DataProvider
    protected static Object[][] dateTimeProvider()
      {
        return new Object[][]
          {
            {ymdhms(2015, 11, 20, 16, 14, 33)}, // normal
            {ymdhms(2015,  8, 20, 16, 14, 33)}, // normal
            {ymdhms(2015,  3, 29,  2, 30,  0)}, // doesn't exist: CET->CEST transition
          };
      }

    private static LocalDateTime ymdhms (int year, int month, int day, int hour, int minute, int second)
      {
        return new LocalDateTime(year, month, day, hour, minute, second);
      }
  }
