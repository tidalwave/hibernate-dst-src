/*
 * #%L
 * *********************************************************************************************************************
 *
 * git clone https://tidalwave@bitbucket.org/tidalwave/hibernate-dst-src.git
 * %%
 * Copyright (C) 2015 - 2015 Tidalwave s.a.s. (http://tidalwave.it)
 * %%
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * $Id$
 *
 * *********************************************************************************************************************
 * #L%
 */
package it.tidalwave.hibernatedst;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import org.joda.time.LocalDateTime;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import static org.junit.Assert.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/***********************************************************************************************************************
 *
 * @author  Fabrizio Giudici Fabrizio.Giudici@tidalwave.it
 * @version $Id$
 *
 **********************************************************************************************************************/
public class HibernateDstTest extends DateTimes
  {
    private final static Logger log = LoggerFactory.getLogger(HibernateDstTest.class);

    private EntityManagerFactory emf;

    private ClassPathXmlApplicationContext context;

    @BeforeClass
    public void setUpClass()
      throws Exception
      {
        context = new ClassPathXmlApplicationContext("META-INF/PersistenceBeans.xml");
        emf = context.getBean(EntityManagerFactory.class);
      }

    @Test(dataProvider = "dateTimeProvider")
    public void persistence_test (final LocalDateTime expectedLdt)
      {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        final EntityWithDate entity = createEntity(expectedLdt);
        em.persist(entity);
        em.getTransaction().commit();
        em.close();

        em = emf.createEntityManager();
        em.getTransaction().begin();
        final EntityWithDate actualEntity = em.find(EntityWithDate.class, entity.getId());
        final LocalDateTime actualLdt = actualEntity.getDateTime();
        em.getTransaction().commit();
        em.close();

        assertEquals(expectedLdt, actualLdt);

//        em = emf.createEntityManager();
//        em.getTransaction().begin();
//        final List<Object[]> rl = em.createNativeQuery("SELECT * FROM EWD WHERE ID=:id")
//                                    .setParameter("id", actualEntity.getId())
//                                    .getResultList();
//        final String stored = rl.get(0)[1].toString();
//        assertEquals("storage for " + actualLdt.toString("yyyy-MM-dd HH:mm:ss.0"), stored);
//        em.getTransaction().commit();
//        em.close();
    }

    private EntityWithDate createEntity (final LocalDateTime dt)
      {
        final EntityWithDate entity = new EntityWithDate();
        entity.setDateTime(dt);
        return entity;
      }
  }
